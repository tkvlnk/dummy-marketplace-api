import { promises } from 'fs';
import { resolve } from 'path';
import AuthService from '../src/service/auth';
import config from '../src/config';

const USERS_FULL_NAMES = [
  'Tit Kovalenko',
  'Roman Horodianenko',
  'Alexey Podobed',
  'Roman Skrypchenko',
  'Dasha Pravda',
  'Aleksandra Shvec',
  'Evhenyj Klyuchyk',
  'Roman Odnosum',
  'Arina Kysilyova',
  'Olha Cherkasenko',
  'Bohdan Nekhai',
  'Polyshhuk Eldar',
  'Mykola Zhmakin',
  'Oleksandr Voloshenko',
  'Dubovskyj Aleksandr',
  'Serhij Lazarec',
  'Vladyslav Tkach',
  'Anton Ocheret',
  'Andrej Shramko',
  'Kyrylo Kravchuk',
  'Yaroslav Lyxolit',
  'Oleksandr Hushha',
  'Hryshunyn Konstantyn',
  'Aleksandr Hruzynskyj',
  'Mariya Hozyayenko',
  'Kyryll Brovko',
  'Bohdan Bylyk',
  'Raxmanin Andrij',
  'Symona Kuznecova',
  'Khrystiuk Anna',
  'Porozhnii Kyrylo',
  'Kshenskyi Sergii'
] as const;

(async (fullNames: readonly string[]) => {
  const auth = new AuthService(config.get('jwtSecret'));

  const fileRows = await fullNames.reduce(async (accumPromise, fullName) => {
    const [accum, token] = await Promise.all([
      accumPromise,
      auth.generateToken(fullName, {
        expiresIn: '60d'
      })
    ]);

    return [...accum, [fullName, token].join(', ')];
  }, Promise.resolve([['Full Name', 'Access Token'].join(', ')]));

  const fileString = fileRows.join('\n');

  await promises.writeFile(resolve(process.cwd(), 'access-tokens.csv'), fileString);

  console.log('File created');
})(USERS_FULL_NAMES);
