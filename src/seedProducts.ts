import { Connection } from 'typeorm';
import * as faker from 'faker';
import ProductEntity, { ProductOrigin } from './entity/product';

export default async function seedProducts(
  connection: Connection,
  count: number
) {
  const productRepo = connection.getRepository(ProductEntity);

  const [, currCount] = await productRepo.findAndCount();

  if (currCount >= count) {
    return;
  }

  for (let i = currCount; i < count; i += 1) {
    const productToSave = Object.assign(new ProductEntity(), {
      name: faker.commerce.productName(),
      origin: faker.helpers.randomize(Object.values(ProductOrigin)),
      price: faker.random.number({ min: 10, max: 1000 })
    } as Partial<ProductEntity>);

    await productRepo.save(productToSave);
  }
}
