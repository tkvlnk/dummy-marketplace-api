import { Connection } from 'typeorm';
import Koa, { BaseContext } from 'koa';
import { RouterContext } from 'koa-router';

declare global {
  interface AppContext extends BaseContext, RouterContext, Koa.Context {
    dbConnection: Connection;
  }

  interface AuthUser {
    fullName: string;
  }

  interface AppState {
    user: AuthUser;
  }

  interface CollectionResponse<R> {
    items: R[];
  }
}
