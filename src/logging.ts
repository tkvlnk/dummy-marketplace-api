import winston from 'winston';
import { Middleware } from 'koa';
import config from './config';

export default function logger(
  winstonInstance: typeof winston
): Middleware<{}, AppContext> {
  return async (ctx, next) => {
    const start = new Date().getMilliseconds();

    await next();

    const ms = new Date().getMilliseconds() - start;

    let logLevel = '';

    if (ctx.status >= 500) {
      logLevel = 'error';
    }
    if (ctx.status >= 400) {
      logLevel = 'warn';
    }
    if (ctx.status >= 100) {
      logLevel = 'info';
    }

    const msg = `${ctx.method} ${ctx.originalUrl} ${ctx.status} ${ms}ms`;

    winstonInstance.configure({
      level: config.get('env') === 'development' ? 'debug' : 'info',
      transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple()
          )
        })
      ]
    });

    winstonInstance.log(logLevel, msg);
  };
}
