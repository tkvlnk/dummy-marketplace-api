import convict from 'convict';
import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV'
  },
  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 3000,
    env: 'PORT',
    arg: 'port'
  },
  dbUrl: {
    doc: 'Database URL.',
    format: 'url',
    default: 'postgres://user:pass@localhost:5432/apidb',
    env: 'DATABASE_URL',
    arg: 'db-url'
  },
  jwtSecret: {
    doc: 'JWT Secret',
    format: String,
    default: 'secret',
    env: 'JWT_SECRET'
  }
});

export default config;
