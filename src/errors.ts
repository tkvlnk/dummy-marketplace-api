import { ValidationError } from 'class-validator';
import { Middleware } from 'koa';
import config from './config';

export enum AppErrorType {
  Unexpected = 'unexpected_exception',
  Validation = 'invalid_input',
  Authorization = 'unauthorized',
  EntityNotFound = 'entity_not_found',
  DataBaseQueryFailure = 'database_query_failure'
}

export enum AppErrorKey {
  InternalServerError = 'internal_server_error',
  InvalidAccessToken = 'invalid_access_token',
  InvalidCreateProductInput = 'invalid_create_product_input',
  InvalidUpdateProductInput = 'invalid_update_product_input',
  ProductUpdateNotAllowed = 'product_update_not_allowed',
  ProductDeleteNotAllowed = 'product_delete_not_allowed',
  NoDuplicatedProductsInOrder = 'no_duplicated_products_in_order',
  InvalidPaginationParams = 'invalid_pagination_params'
}

interface AppErrorProps {
  sourceError?: Error;
  validationErrors?: ValidationError[];
}

const errorIdsMap: {
  [key in AppErrorKey]: {
    renderMessage: (props: AppErrorProps) => string;
    status: number;
  };
} = {
  [AppErrorKey.InternalServerError]: {
    status: 500,
    renderMessage: () => 'Internal server error'
  },
  [AppErrorKey.InvalidAccessToken]: {
    status: 401,
    renderMessage: () => 'Invalid access token'
  },
  [AppErrorKey.InvalidCreateProductInput]: {
    status: 400,
    renderMessage: () => 'Cannot create product using given data'
  },
  [AppErrorKey.InvalidUpdateProductInput]: {
    status: 400,
    renderMessage: () => 'Cannot update product using given data'
  },
  [AppErrorKey.ProductUpdateNotAllowed]: {
    status: 403,
    renderMessage: () => 'Not allowed to update this product'
  },
  [AppErrorKey.ProductDeleteNotAllowed]: {
    status: 403,
    renderMessage: () => 'Not allowed to delete this product'
  },
  [AppErrorKey.NoDuplicatedProductsInOrder]: {
    status: 400,
    renderMessage: () => 'Order cannot contain duplicated product ids'
  },
  [AppErrorKey.InvalidPaginationParams]: {
    status: 400,
    renderMessage: () => 'Page and per page must be positive if specified'
  }
};

export default class AppError extends Error {
  isAppError = true;

  status: number;

  message: string;

  key: AppErrorKey;

  constructor(
    key: AppErrorKey | string,
    public type: AppErrorType = AppErrorType.Unexpected,
    public props: AppErrorProps = {}
  ) {
    super();

    const errorConfig = errorIdsMap[key as AppErrorKey];

    if (errorConfig) {
      const { status, renderMessage } = errorConfig;

      this.status = status;
      this.message = renderMessage(props);
      this.key = key as AppErrorKey;
    } else {
      this.status = 500;
      this.message = key;
      this.key = AppErrorKey.InternalServerError;
    }

    this.message = this.message
      .replace(/^./, match => match.toUpperCase())
      .replace(/.$/, match => (match === '.' ? match : `${match}.`));
  }

  setStatus(status: number) {
    this.status = status;

    return this;
  }

  static is(candidate: { isAppError?: boolean }): candidate is AppError {
    return candidate?.isAppError === true;
  }

  static format(e: Error): AppError {
    if (e.name === 'EntityNotFound') {
      return new AppError(
        e.message as AppErrorKey,
        AppErrorType.EntityNotFound
      ).setStatus(404);
    }

    if (e.name === 'QueryFailedError') {
      return new AppError(
        e.message as AppErrorKey,
        AppErrorType.DataBaseQueryFailure
      ).setStatus(400);
    }

    return new AppError(e.message as AppErrorKey, AppErrorType.Unexpected, {
      sourceError: e
    });
  }

  toJSON() {
    const result = { ...this };

    if (result.props.sourceError && config.get('env') !== 'development') {
      delete result.props.sourceError.stack;
    }

    if (result.props.validationErrors) {
      for (const validationError of result.props.validationErrors) {
        delete validationError.target;
      }
    }

    delete result.isAppError;

    return result;
  }
}

export const catchErrors: Middleware = async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    const error = AppError.is(e) ? e : AppError.format(e);

    ctx.body = {
      error
    };
    ctx.status = error.status;
  }
};
