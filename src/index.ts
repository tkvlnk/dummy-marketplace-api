import 'reflect-metadata';

import Koa from 'koa';

import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';
import { ConnectionOptions, parse } from 'pg-connection-string';
import { Connection, createConnection } from 'typeorm';
import { ui } from 'swagger2-koa';
import helmet from 'koa-helmet';
import winston from 'winston';

import config from './config';
import ProductEntity from './entity/product';
import OrderEntity from './entity/order';
import seedProducts from './seedProducts';
import logger from './logging';
import specDocument from './docs';
import ProductsService from './service/products';
import OrderPieceEntity from './entity/order-piece';
import OrdersService from './service/orders';
import AuthService from './service/auth';
import { catchErrors } from './errors';
import testTaskMiddleware from './task0';

(async () => {
  const connectionOptions = parse(config.get('dbUrl')) as Record<
    keyof ConnectionOptions,
    string
  >;

  let dbConnection: Connection;

  try {
    dbConnection = await createConnection({
      type: 'postgres',
      host: connectionOptions.host,
      port: parseInt(connectionOptions.port, 10),
      username: connectionOptions.user,
      password: connectionOptions.password,
      database: connectionOptions.database,
      synchronize: true,
      logging: false,
      entities: [ProductEntity, OrderEntity, OrderPieceEntity],
      extra: {
        ssl: config.get('env') !== 'development'
      }
    });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('Postgres connection error:', err.message);
    return;
  }

  await seedProducts(dbConnection, 200);

  const app = new Koa();

  app.context.dbConnection = dbConnection;

  const apiBase = '/api';
  const v1Base = '/v1';

  const basePath = [apiBase, v1Base].join('');

  const auth = new AuthService(config.get('jwtSecret'));
  const product = new ProductsService(dbConnection, basePath, auth);
  const order = new OrdersService(dbConnection, basePath, auth);

  app
    .use(helmet())
    .use(cors())
    .use(logger(winston))
    .use(bodyParser())
    .use(ui(await specDocument, apiBase, [basePath, '/api/task0']))
    .use(catchErrors)
    .use(auth.decodeUser())
    .use(product.middleware())
    .use(order.middleware())
    .use(testTaskMiddleware)
    .use(async (ctx, next) => {
      try {
        await next();
      } catch (err) {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        ctx.app.emit('error', err, ctx);
      }
    });

  const port = config.get('port');

  app.listen(port, () => {
    if (process.env.NODE_ENV === 'production') {
      return;
    }

    // eslint-disable-next-line no-console
    console.log(`🚀 Running on http://localhost:${port}${apiBase}`);
  });
})();
