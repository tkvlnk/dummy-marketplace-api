import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsString,
  MaxLength,
  MinLength
} from 'class-validator';

export enum ProductOrigin {
  Europe = 'europe',
  Usa = 'usa',
  Africa = 'africa',
  Asia = 'asia'
}

@Entity({
  name: 'product'
})
export default class ProductEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({
    unique: true
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(20)
  name!: string;

  @Column()
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  price!: number;

  @Column()
  @IsNotEmpty()
  @IsEnum(ProductOrigin)
  origin!: ProductOrigin;

  @Column({
    nullable: true
  })
  @IsString()
  createdBy?: string;

  @Column({
    default: false
  })
  isDeleted!: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt!: string;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt!: string;
}
