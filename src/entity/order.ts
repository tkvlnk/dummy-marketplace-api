import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { IsString } from 'class-validator';
import OrderPieceEntity from './order-piece';

@Entity({
  name: 'order'
})
export default class OrderEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @OneToMany(
    () => OrderPieceEntity,
    productInOrder => productInOrder.order,
    { eager: true }
  )
  @JoinTable()
  pieces!: OrderPieceEntity[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt!: string;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt!: string;

  @Column()
  @IsString()
  createdBy?: string;
}
