import {
  Column,
  Entity,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { IsInt, IsPositive } from 'class-validator';
import ProductEntity from './product';
import OrderEntity from './order';

@Entity({
  name: 'order_piece'
})
export default class OrderPieceEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(
    () => OrderEntity,
    order => order.pieces
  )
  order!: OrderEntity;

  @ManyToOne(() => ProductEntity, {
    eager: true
  })
  @JoinTable()
  product!: ProductEntity;

  @Column()
  @IsInt()
  @IsPositive()
  count!: number;
}
