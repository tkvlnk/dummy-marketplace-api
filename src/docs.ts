import { SpecGenerator } from 'typescript-rest-swagger/dist/swagger/generator';
import { MetadataGenerator } from 'typescript-rest-swagger/dist/metadata/metadataGenerator';

import { version } from '../package.json';

const entryFile = ['./src/service/*.ts'];

const specDocument = new SpecGenerator(
  new MetadataGenerator(entryFile, {}).generate(),
  {
    name: 'Yalantis React School API',
    version,
    basePath: '/api/v1',
    outputDirectory: './docs',
    entryFile,
    yaml: false,
    securityDefinitions: {
      BaseAuth: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization'
      }
    }
  }
).getOpenApiSpec();

export default specDocument;
