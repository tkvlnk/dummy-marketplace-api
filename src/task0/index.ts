import { createReadStream } from 'fs';
import { join } from 'path';
import Router from 'koa-router';
import compose from 'koa-compose';

const testTaskRouter = new Router<{}, AppContext>();

testTaskRouter.get('/api/task0/users', ctx => {
  ctx.set('Content-Type', 'application/json');
  ctx.body = createReadStream(join(__dirname, 'users-data.json'));
});

const testTaskMiddleware = compose([
  testTaskRouter.routes(),
  testTaskRouter.middleware()
]);

export default testTaskMiddleware;
