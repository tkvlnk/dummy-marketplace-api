import {
  Context,
  DELETE,
  GET,
  PATCH,
  POST,
  Path,
  PathParam,
  QueryParam,
  Security
} from 'typescript-rest';
import { Tags } from 'typescript-rest-swagger';
import { Between, Connection, FindOneOptions, In, IsNull, Not } from 'typeorm';
import qs from 'qs';

import { validate } from 'class-validator';
import ProductEntity, { ProductOrigin } from '../entity/product';
import AbstractService from './abstract';
import AuthService from './auth';
import AppError, { AppErrorKey, AppErrorType } from '../errors';

export interface Product {
  id: string;
  name: string;
  price: number;
  origin: ProductOrigin;
  createdAt: string;
  updatedAt: string;
  isEditable: boolean;
}

function parseBoolQueryParam(boolStr: string | undefined): boolean | undefined {
  let result: boolean | undefined;

  switch (true) {
    case boolStr === 'true':
      result = true;
      break;
    case boolStr === 'false':
      result = false;
      break;
    default:
      result = undefined;
  }

  return result;
}

@Path('')
@Tags('Products')
@Security([], 'BaseAuth')
export default class ProductsService extends AbstractService {
  routesPrefix = 'products';

  constructor(
    dbConnection: Connection,
    basePath = '',
    private auth: AuthService
  ) {
    super(dbConnection, basePath);

    this.router
      .get('/', async (ctx, next) => {
        const {
          origins,
          page,
          perPage,
          minPrice,
          maxPrice,
          editable
        } = qs.parse(ctx.request.query);

        ctx.body = await this.getProducts(
          ctx.state.user,
          parseInt(page, 10),
          parseInt(perPage, 10),
          origins?.split(','),
          parseInt(minPrice, 10),
          parseInt(maxPrice, 10),
          parseBoolQueryParam(editable)
        );

        return next();
      })
      .post('/', this.auth.guardUnauthorized(), async (ctx, next) => {
        ctx.body = await this.createProduct(ctx.request.body, ctx.state.user);

        return next();
      })
      .get('-origins', (ctx, next) => {
        ctx.body = this.getProductsOrigins();
        return next();
      })
      .get('/:productId', async (ctx, next) => {
        const { productId } = ctx.params;

        ctx.body = await this.getProductById(productId, ctx.state.user);

        return next();
      })
      .patch(
        '/:productId',
        this.auth.guardUnauthorized(),
        async (ctx, next) => {
          const {
            params: { productId },
            request: { body },
            state: { user }
          } = ctx;
          ctx.body = await this.updateProduct(productId, body, user);
          return next();
        }
      )
      .delete(
        '/:productId',
        this.auth.guardUnauthorized(),
        async (ctx, next) => {
          const { productId } = ctx.params;
          ctx.body = await this.deleteProductById(productId, ctx.state.user);
          return next();
        }
      );
  }

  static renderProductFor(
    productEntity: ProductEntity,
    authUser: AuthUser
  ): Product {
    const { createdBy, isDeleted, ...product } = productEntity;

    return {
      isEditable: authUser.fullName === createdBy,
      ...product
    };
  }

  @GET
  @Path('/products')
  public async getProducts(
    @Context authUser: AuthUser,
    @QueryParam('page')
    rawPage?: number,
    @QueryParam('perPage')
    rawPerPage?: number,
    @QueryParam('origins')
    origins?: ProductOrigin[],
    @QueryParam('minPrice')
    rawMinPrice?: number,
    @QueryParam('maxPrice')
    rawMaxPrice?: number,
    @QueryParam('editable')
    isEditable?: boolean
  ): Promise<{
    items: Product[];
    totalItems: number;
    page: number;
    perPage: number;
  }> {
    if ([rawPage, rawPerPage].some(param => param != null && param < 1)) {
      throw new AppError(
        AppErrorKey.InvalidPaginationParams,
        AppErrorType.Validation
      );
    }

    const page = rawPage || 1;
    const perPage = rawPerPage || 50;

    let productConditions: FindOneOptions<ProductEntity>['where'] = {
      isDeleted: false
    };

    if (origins?.filter(Boolean).length) {
      productConditions.origin = In(origins);
    }

    const minPrice = rawMinPrice || 0;
    const maxPrice = Number.isNaN(Number(rawMaxPrice))
      ? 10000
      : Number(rawMaxPrice);

    if (![minPrice, maxPrice].every(Number.isNaN)) {
      productConditions.price = Between(minPrice, maxPrice);
    }

    if (typeof isEditable === 'boolean') {
      if (authUser?.fullName) {
        if (isEditable) {
          productConditions.createdBy = authUser.fullName;
        } else {
          productConditions = [
            { ...productConditions, createdBy: Not(authUser.fullName) },
            { ...productConditions, createdBy: IsNull() }
          ];
        }
      } else {
        productConditions.createdBy = isEditable
          ? [IsNull(), Not(IsNull())]
          : IsNull();
      }
    }

    const [items, totalItems] = await this.dbConnection
      .getRepository(ProductEntity)
      .findAndCount({
        order: {
          createdAt: 'ASC'
        },
        skip: page ? (page - 1) * perPage : 0,
        take: perPage,
        where: productConditions
      });

    return {
      page,
      perPage,
      totalItems,
      items: items.map(product =>
        ProductsService.renderProductFor(product, authUser)
      )
    };
  }

  @GET
  @Path('/products/:productId')
  public async getProductById(
    @PathParam('productId') productId: string,
    @Context authUser: AuthUser
  ): Promise<Product> {
    return ProductsService.renderProductFor(
      await this.dbConnection
        .getRepository(ProductEntity)
        .findOneOrFail(productId, {
          where: {
            isDeleted: false
          }
        }),
      authUser
    );
  }

  @POST
  @Path('/products')
  public async createProduct(
    payload: {
      product: {
        name: string;
        price: number;
        origin: ProductOrigin;
      };
    },
    @Context authUser: AuthUser
  ): Promise<Product> {
    const productRepo = this.dbConnection.getRepository(ProductEntity);

    const draftProduct = productRepo.create({
      createdBy: authUser.fullName,
      name: payload.product.name,
      price: payload.product.price,
      origin: payload.product.origin
    });

    const errors = await validate(draftProduct);

    if (errors.length > 0) {
      throw new AppError(
        AppErrorKey.InvalidCreateProductInput,
        AppErrorType.Validation,
        { validationErrors: errors }
      );
    }

    return ProductsService.renderProductFor(
      await productRepo.save(draftProduct),
      authUser
    );
  }

  @PATCH
  @Path('/products/:productId')
  public async updateProduct(
    @PathParam('productId') productId: string,
    payload: {
      product: {
        name: string;
        price: number;
        origin: ProductOrigin;
      };
    },
    @Context authUser: AuthUser
  ): Promise<Product> {
    const productRepo = this.dbConnection.getRepository(ProductEntity);

    const storedProduct = await productRepo.findOneOrFail(productId);

    if (storedProduct.createdBy !== authUser.fullName) {
      throw new AppError(
        AppErrorKey.ProductUpdateNotAllowed,
        AppErrorType.Authorization
      );
    }

    const draftProduct = productRepo.merge(storedProduct, {
      name: payload.product.name,
      price: payload.product.price,
      origin: payload.product.origin
    });

    const errors = await validate(draftProduct);

    if (errors.length > 0) {
      throw new AppError(
        AppErrorKey.InvalidUpdateProductInput,
        AppErrorType.Validation,
        {
          validationErrors: errors
        }
      );
    }

    return ProductsService.renderProductFor(
      await productRepo.save(draftProduct),
      authUser
    );
  }

  @DELETE
  @Path('/products/:productId')
  public async deleteProductById(
    @PathParam('productId') productId: string,
    @Context authUser: AuthUser
  ) {
    const productRepo = this.dbConnection.getRepository(ProductEntity);

    const storedProduct = await productRepo.findOneOrFail(productId);

    if (storedProduct.createdBy !== authUser.fullName) {
      throw new AppError(
        AppErrorKey.ProductDeleteNotAllowed,
        AppErrorType.Authorization
      );
    }

    await productRepo.update(productId, {
      name: `${storedProduct.name}[deleted-at:${new Date().toISOString()}]`,
      isDeleted: true
    });
  }

  @GET
  @Path('/products-origins')
  public getProductsOrigins(): {
    items: {
      displayName: string;
      value: ProductOrigin;
    }[];
  } {
    return {
      items: [
        {
          value: ProductOrigin.Africa,
          displayName: 'Africa'
        },
        {
          value: ProductOrigin.Asia,
          displayName: 'Asia'
        },
        {
          value: ProductOrigin.Europe,
          displayName: 'Europe'
        },
        {
          value: ProductOrigin.Usa,
          displayName: 'USA'
        }
      ]
    };
  }
}
