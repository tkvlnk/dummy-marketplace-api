import { Context, GET, POST, Path, PathParam, Security } from 'typescript-rest';
import { Connection } from 'typeorm';
import { Tags } from 'typescript-rest-swagger';
import { validate } from 'class-validator';
import ProductsService, { Product } from './products';
import OrderEntity from '../entity/order';
import ProductEntity from '../entity/product';
import OrderPieceEntity from '../entity/order-piece';
import AbstractService from './abstract';
import AuthService from './auth';
import AppError, { AppErrorKey, AppErrorType } from '../errors';

interface Order {
  id: string;
  pieces: {
    product: Product;
    count: number;
  }[];
  createdAt: string;
}

@Path('orders')
@Tags('Orders')
@Security([], 'BaseAuth')
export default class OrdersService extends AbstractService {
  routesPrefix = 'orders';

  constructor(
    dbConnection: Connection,
    basePath = '',
    private auth: AuthService
  ) {
    super(dbConnection, basePath);

    this.router
      .use(this.auth.guardUnauthorized())
      .get('/', async ctx => {
        ctx.body = await this.getOrders(ctx.state.user);
      })
      .post('/', async ctx => {
        ctx.body = await this.createOrder(ctx.request.body, ctx.state.user);
      })
      .get('/:orderId', async ctx => {
        const { orderId } = ctx.params;
        ctx.body = await this.getOrderById(orderId, ctx.state.user);
      });
  }

  static renderOrderFor(orderEntity: OrderEntity, authUser: AuthUser): Order {
    const { pieces, createdBy, updatedAt, ...order } = orderEntity;

    return {
      ...order,
      pieces: pieces.map(({ product, ...rest }) =>
        Object.assign(rest, {
          order: undefined,
          product: ProductsService.renderProductFor(product, authUser)
        })
      )
    };
  }

  @POST
  async createOrder(
    payload: {
      order: {
        pieces: {
          productId: string;
          count: number;
        }[];
      };
    },
    @Context authUser: AuthUser
  ): Promise<Order> {
    return this.dbConnection.transaction(async manager => {
      const orderRepo = manager.getRepository(OrderEntity);

      const draftOrder = orderRepo.create({
        createdBy: authUser.fullName
      });

      const { id: orderId } = await orderRepo.save(draftOrder);

      if (!isUniqueProductIds()) {
        throw new AppError(
          AppErrorKey.NoDuplicatedProductsInOrder,
          AppErrorType.Validation
        );
      }

      const pieces = await Promise.all(
        payload.order.pieces.map(async ({ productId, count }) => {
          const orderPieceRepo = manager.getRepository(OrderPieceEntity);

          const orderPiece = orderPieceRepo.create({
            product: {
              id: productId
            },
            order: {
              id: orderId
            },
            count
          });

          const errors = await validate(orderPiece);

          if (errors.length > 0) {
            throw new Error('Invalid order piece');
          }

          await this.dbConnection
            .getRepository(ProductEntity)
            .findOneOrFail(productId);

          return orderPieceRepo.save(orderPiece);
        })
      );

      return OrdersService.renderOrderFor(
        await manager.getRepository(OrderEntity).save({
          id: orderId,
          pieces,
          createdBy: authUser.fullName
        }),
        authUser
      );
    });

    function isUniqueProductIds() {
      const productIds = payload.order.pieces.map(({ productId }) => productId);

      return new Set(productIds).size === productIds.length;
    }
  }

  @GET
  async getOrders(
    @Context authUser: AuthUser
  ): Promise<{
    items: Order[];
  }> {
    return {
      items: (
        await this.dbConnection.getRepository(OrderEntity).find({
          where: {
            createdBy: authUser.fullName
          }
        })
      ).map(order => OrdersService.renderOrderFor(order, authUser))
    };
  }

  @GET
  @Path(':orderId')
  async getOrderById(
    @PathParam('orderId') orderId: string,
    @Context authUser: AuthUser
  ): Promise<Order> {
    return OrdersService.renderOrderFor(
      await this.dbConnection
        .getRepository(OrderEntity)
        .findOneOrFail(orderId, {
          where: {
            createdBy: authUser.fullName
          }
        }),
      authUser
    );
  }
}
