import { Connection } from 'typeorm';
import compose from 'koa-compose';
import Router from 'koa-router';

export default abstract class AbstractService {
  router = new Router<AppState, AppContext>();

  routesPrefix = '';

  constructor(protected dbConnection: Connection, private basePath: string) {}

  middleware() {
    this.router.prefix([this.basePath, this.routesPrefix].join('/'));
    return compose([this.router.routes(), this.router.allowedMethods()]);
  }
}
