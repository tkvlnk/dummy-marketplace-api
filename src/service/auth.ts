import { promisify } from 'util';

import { Secret, SignOptions, decode, sign, verify } from 'jsonwebtoken';
import { Middleware } from 'koa';
import AppError, { AppErrorKey, AppErrorType } from '../errors';

export interface JwtPayload {
  iat: number;
  exp: number;
  fullName: string;
}

export default class AuthService {
  constructor(private readonly jwtSecret: string) {}

  public async generateToken(
    fullName: string,
    options: SignOptions = {}
  ): Promise<string> {
    const jwtPayload: Omit<JwtPayload, 'iat' | 'exp'> = {
      fullName
    };

    return promisify<string | Buffer | object, Secret, SignOptions, string>(
      sign
    )(jwtPayload, this.jwtSecret, options);
  }

  public async getTokenPayload(token: string) {
    await promisify(verify)(token, this.jwtSecret);

    return decode(token);
  }

  public decodeUser = (): Middleware<AppState, AppContext> => {
    return async (ctx, next) => {
      try {
        ctx.state.user = await this.getTokenPayload(
          ctx.request.headers.authorization
        );

        return next();
      } catch (e) {
        ctx.state.user = {};

        return next();
      }
    };
  };

  public guardUnauthorized = (): Middleware<AppState, AppContext> => {
    return async (ctx, next) => {
      if (!ctx.state.user?.fullName) {
        throw new AppError(
          AppErrorKey.InvalidAccessToken,
          AppErrorType.Authorization
        );
      }

      return next();
    };
  };
}
